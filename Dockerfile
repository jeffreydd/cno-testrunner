FROM docker:19.03.12

RUN apk add --no-cache \
    python3 python3-dev py3-pip gcc git curl build-base \
    autoconf automake py3-cryptography linux-headers \
    musl-dev libffi-dev openssl-dev openssh
RUN python3 -m pip install ansible molecule[docker] openshift==v0.11.2 jmespath skopeo-bin boto3 minio s3cmd kubernetes-validate
